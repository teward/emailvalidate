## emailvalidate - a Python email validation library

### Basic Information

<table>
<tr><td align=center valign=center><a href="http://www.gnu.org/licenses/gpl-3.0" target="_blank">
<img src="https://img.shields.io/badge/license-GPLv3-blue" title="GPLv3" /></a></td></tr>
<!--<tr><td align=center valign=center><a href="https://pypi.python.org/pypi/emailvalidate" target="_blank"><img "https://img.shields.io/pypi/v/emailvalidate" title="PyPI Version" /></a></td></tr>-->
</table>


### Continuous Integration Status

| CI Provider   | Status                                                                                                                                            |
|:-------------:|:-------------------------------------------------------------------------------------------------------------------------------------------------:|
| GitLab CI     | [![Pipeline Status](https://gitlab.com/teward/emailvalidate/badges/master/pipeline.svg)](https://gitlab.com/teward/emailvalidate/commits/master)  |


## Description

This module is based on the older, less-maintained and less developer-use-friendly 
[`validate_email`](https://pypi.org/project/validate_email/) project.

In needing to validate email addresses, there aren't many mechanisms to test email address validity.
The original `validate_email` project is 'good' for syntax verification, but does not include other 
validation mechanisms, and cannot actually be used sanely in a programmatic way to validate an email 
against the actual email servers and doesn't report specific information that someone might desire. 

Therefore, this module was created. It is driven by concepts that `validate_email` inspires, but 
adds additional functionality, validations, and mechanisms that are superior to the mechanisms in 
the original module which at the time of this library being written has not been updated 
since 2015.

This module provides programmer-usable functionality to test if an email is RFC syntatically valid, 
whether the domain specified in an email address is a potentially valid domain (optional), whether 
the domain specified in an address actually has MX records and whether any are reachable (optional),
and also whether or not the specified email address/recipient actually exists on the MX servers for 
the domain (optional).


## Compatibility 

This module is written for Python 3.6 or newer, and uses compents specifically included in Python 
versions 3.6 and up. Older versions of Python are not supported.

CI tests are run regularly on 3.6 and 3.7, as well as with Pylint to make sure the code is 'linted'.


## Installation / Usage

### Use PyPI

This module is available in the PyPI repositories.

    python3 -m pip install --upgrade emailvalidate
    
### From Source Code

#### Dependencies

First, install the dependencies from PyPI.

For system-wide installation:

    python3 -m pip install --upgrade -r requirements.txt

For user-space installation:

    python3 -m pip install --user --upgrade -r requirements.txt
    
### Installing / Importing in Code

Simply copy the `imaplibext` package folder into your working directory for your Python script or 
program.


## Usage

Once installed, you may import this library by doing:

    import emailvalidate
    
Alternatively, if you want to specifically import the only public method available, use:

    from emailvalidate import validate_email_address
    
## Function Details

The public function `validate_email_address` has the following usage layout:

    validate_email_address(email, smtp_timeout=10, validate_tld=False, check_mx=False, verify=False)
    
The arguments provided to this function are as follows:

 - `email` - required - the email address as a string to validate for syntax and other optional 
   tests.
 - `smtp_timeout` - optional - default: `10` - the time in seconds for SMTP connection attempts to 
   time out when `check_mx` or `verify` are set to `True`.
 - `validate_tld` - optional - default: `False` - Indicates whether to validate the domain 
   specified in an email address for whether it contains a potentially valid TLD. Requires the 
   `tld` library to be installed.
 - `check_mx` - optional - default: `False` - Indicates whether to check to see if the domain of 
   the email address has MX records and if at least one of the MX records is reachable. If set to 
   `True`, this forces the `validate_tld` option to be `True`. If TLD validation fails, the function 
   does not bother testing for MX records or if MX servers are reachable.
 - `verify` - optional - default: `False` - Indicates whether to test valid MX records for the email 
   address's domain to determine whether the recipient is valid for the domain. If set to `True`, 
   this will automatically enforce that `validate_tld` and `check_mx` are set to `True`.  If TLD 
   vaidation or MX checking fails, this test does nothing additional.
   
This function returns results in a `dict` that ascribes to the following format:

    {
        'syntax': True,
        'valid_tld': True,
        'mx_exists': True,
        'email_exists': True
    }
    
Details on the specific values are specified in the Verification Process section below.
        
   
## Verification Process

### Syntax

The first action in the function here will validate the syntax against RFC 2822 syntax requirements 
for an email address. This does so with regex detections.  This test is always enabled.

This test drives the `syntax` key in the result dictionary. If the address is syntatically valid, 
then the value is `True`. Otherwise, the value is `False`.

If this test fails, then regardless of whether TLD validation, MX checks, or email existence 
verification is enabled or not, there will be no other results in the results.

### TLD Validation

If the `tld` library is present, then this function will extract the domain out of the email 
address (the part to the right of the `@`) and validate it to determine if it's a potentially 
valid domain.

This test drives the `valid_tld` key in the result dictionary. If the domain extracted from the 
email address is a valid TLD, then the result is `True`, otherwise it will be `False`.

TLD validation is required for MX checks and for email recipient verification. If TLD checks fail, 
then regardless of whether MX checks or email recipient verification is enabled or not, then only 
the `syntax` and `valid_tld` keys will be present in the output.

### MX existence checks

If the `py3dns` library is present, then this function will attempt to retrieve MX records from 
DNS for the domain, and then test that at least one of the MX records is reachable.

This check drives the `mx_exists`key in the results.

If DNS lookups return `NXDOMAIN` or returns no `MX` records or DNS lookups fail for some reason, 
the result for this test will be `False`.

If DNS lookups return MX records, but no MX servers are reachable from this library's run location 
then this test will return `False`.

If DNS lookups return MX records, and at least one MX server is reachable, this test will return 
`True`.

MX checks are required for email recipient validation. If MX checks fail, then regardless if 
`verify` is set to `True` or not, the only results in the result dictionary will be `syntax`, 
`valid_tld`, and `mx_exists`.

### Email recipient verification

The final check this function provides is the ability to verify that a specified email address is 
actually a valid recipient for that domain and the mail servers in DNS. This can be especially 
helpful for listserv validation of a recipient *before* sending a message to the recipient.

Email recipient verification requires syntax, TLD verification, and MX existence tests to all 
successfuly pass.  If they do not pass, then `email_exists` does not get included in result sets.

If enabled, then one of the tricks handled by the MX checks is to also provide which MX server 
(internally) successfully was reached based on DNS records. This MX server's host name is provided 
to the internal verification system.

This check will connect to the MX server and attempt to use STARTTLS to establish a secure 
connection. If STARTTLS fails, it falls back to plain connection. It then establishes a `MAIL` call
and then makes a `RCPT` call with the specific email address in the recipient.

Based on the response call to the `RCPT TO` inquiry, (either `250 Ok` or not), the verification 
will either succeed or fail.

For any SMTP response of "OK" with code `250`, indicating the recipient is valid, this test will 
set `email_exists` to `True` in the result dictionary.

For any non-`250` response code or connection error, this test will set `email_exists` to `False` 
in the result dictionary.

## Example Results

> These examples all assume that the `validate_tld`, `check_mx`, and `verify` options were set 
to `True`

If an email address is syntatically correct, the domain in the email address is a potentially valid 
TLD, but has no reachable MX records or has no records, the resultant dictionary will be:

    {'syntax': True, 'valid_tld': True, 'mx_exists': False}
 
If an email address is syntatically correct but the domain specified in the email address is not a 
valid TLD, the resultant dictionary will be:

    {'syntax': True, 'valid_tld': False}
 
If an email address is syntatically correct, the domain is a potentially valid TLD, has MX records, 
but is not a valid recipient (the return code is not `250`), the resultant dictionary will be:

    {'syntax': True, 'valid_tld': True, 'mx_exists': True, 'email_exists': False}
    
For a syntatically correct email address, a valid TLD, reachable MX servers, and a valid recipient, 
the resultant dictionary will be:

    {'syntax': True, 'valid_tld': True, 'mx_exists': True, 'email_exists': True}


## FAQ

### Where can I report issues or make Feature Requests?

Issues and feature requests can be reported on the 
[Gitlab Repository](https://gitlab.com/teward/emailvalidate).