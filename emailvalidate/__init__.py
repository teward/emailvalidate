"""emailvalidate

This module provides an email validation library to validate email addresses.

The core module code is in _emailvalidate.py
"""

from ._emailvalidate import validate_email_address  # pylint: disable=import-error

__title__ = 'emailvalidate'
__author__ = 'Thomas Ward'
__version__ = '0.1.3'
__copyright__ = 'Copyright 2019, Thomas Ward'
__license__ = 'GPLv3'
__all__ = validate_email_address
