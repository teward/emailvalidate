"""
emailvalidate

This module provides an email validation library to validate email addresses.
"""

import smtplib
from typing import Union, Tuple, List, Dict
import socket
import regex

String = str

try:
    import tld
    tld.update_tld_names(fail_silently=True)
except (ImportError, AttributeError):
    tld = None

try:
    # noinspection PyPackageRequirements
    import DNS  # provided py py3dns
    ServerError = DNS.ServerError
    DNS.DiscoverNameServers()
except (ImportError, AttributeError):
    DNS = None

    class ServerError(Exception):
        """
        Basic ServerError for when we raise or catch ServerError while py3dns is unavailable
        """


class Tokens:  # pylint: disable=too-few-public-methods
    """
    This class simply holds predefined strings which together comprise regex components
    for RFC2822-compliant syntax for an email address.

    This set of tokens is syntaxically valid as of 2011 syntaxes; email syntax hasn't changed
    much in that time.
    """
    # see 2.2.2. Structured Header Field Bodies
    WSP = r'[\s]'

    # see 2.2.3. Long Header Fields
    CRLF = r'(?:\r\n)'

    # see 3.2.1. Primitive Tokens
    NO_WS_CTL = r'\x01-\x08\x0b\x0c\x0f-\x1f\x7f'

    # see 3.2.2. Quoted characters
    QUOTED_PAIR = r'(?:\\.)'

    # see 3.2.3. Folding white space and comments
    FWS = r'(?:(?:' + WSP + r'*' + CRLF + r')?' + WSP + r'+)'
    CTEXT = r'[' + NO_WS_CTL + r'\x21-\x27\x2a-\x5b\x5d-\x7e]'
    # (NB: The RFC includes COMMENT here as well, but that would be circular.)
    CCONTENT = r'(?:' + CTEXT + r'|' + QUOTED_PAIR + r')'
    COMMENT = r'\((?:' + FWS + r'?' + CCONTENT + r')*' + FWS + r'?\)'
    CFWS = r'(?:' + FWS + r'?' + COMMENT + ')*(?:' + FWS + '?' + COMMENT + '|' + FWS + ')'

    # see 3.2.4. Atom
    ATEXT = r'[\w!#$%&\'\*\+\-/=\?\^`\{\|\}~]'
    ATOM = CFWS + r'?' + ATEXT + r'+' + CFWS + r'?'
    DOT_ATOM_TEXT = ATEXT + r'+(?:\.' + ATEXT + r'+)*'
    DOT_ATOM = CFWS + r'?' + DOT_ATOM_TEXT + CFWS + r'?'

    # see 3.2.5. Quoted strings
    QTEXT = r'[' + NO_WS_CTL + r'\x21\x23-\x5b\x5d-\x7e]'
    QCONTENT = r'(?:' + QTEXT + r'|' + QUOTED_PAIR + r')'
    QUOTED_STRING = CFWS + r'?' + r'"(?:' + FWS + \
        r'?' + QCONTENT + r')*' + FWS + \
        r'?' + r'"' + CFWS + r'?'

    # see 3.4.1. Addr-spec specification
    LOCAL_PART = r'(?:' + DOT_ATOM + r'|' + QUOTED_STRING + r')'
    DTEXT = r'[' + NO_WS_CTL + r'\x21-\x5a\x5e-\x7e]'
    DCONTENT = r'(?:' + DTEXT + r'|' + QUOTED_PAIR + r')'
    DOMAIN_LITERAL = CFWS + r'?' + r'\[' + \
        r'(?:' + FWS + r'?' + DCONTENT + \
        r')*' + FWS + r'?\]' + CFWS + r'?'
    DOMAIN = r'(?:' + DOT_ATOM + r'|' + DOMAIN_LITERAL + r')'
    ADDR_SPEC = LOCAL_PART + r'@' + DOMAIN

    # A valid address will match exactly the 3.4.1 addr-spec.
    VALID_ADDRESS_REGEXP = '^' + ADDR_SPEC + '$'


# DNS Cache bits get stored here:
MX_DNS_CACHE = {}
MX_CHECK_CACHE = {}


def _get_mx_ip(hostname: String) -> List:  # pylint: disable=E0001
    # Internal function that gets MX records for a specified FQDN.
    #
    # Note on pylint disable of E0001: It invalidly detects a string syntax error here.

    if not DNS:
        raise ModuleNotFoundError("The DNS module is unavailable, please install py3DNS to "
                                  "validate MX records exist.")

    if hostname not in MX_DNS_CACHE:
        try:
            records = DNS.mxlookup(hostname)
            MX_DNS_CACHE[hostname] = list(zip(*records))[1]
        except ServerError as error:
            if error.rcode in [2, 3]:  # SERVFAIL, NXDOMAIN (NonExistent Domain), respectively
                MX_DNS_CACHE[hostname] = None
            else:
                raise

    return MX_DNS_CACHE[hostname]


def _validate_email(address: String, validate_tld: bool = False) -> Dict:
    # Internal mechanism to validate email syntax and (optionally) the email domain against
    # valid TLDs.

    validation = {'syntax': bool(regex.match(Tokens.VALID_ADDRESS_REGEXP, address, regex.I))}

    if validate_tld and tld:
        validation['domain'] = bool(tld.get_fld(address.split('@')[1],
                                                fix_protocol=True,
                                                fail_silently=True))

    return validation


def _validate_mx_exists(address: String, email_verification_pending: bool = False,
                        smtp_timeout: int = 15) -> Tuple:
    # Relies on _get_mx_ip.
    #
    # Internal function used to test MX records to validate we can *reach* remote mail servers
    # for a specified email address.

    # noinspection PyBroadException
    try:
        mx_hosts = _get_mx_ip(address.split('@')[1])
    except Exception:  # pylint: disable=broad-except
        return False, None

    if not mx_hosts:
        return False, None

    smtp = smtplib.SMTP(timeout=smtp_timeout)

    for mx in mx_hosts:
        try:
            if not email_verification_pending and mx in MX_CHECK_CACHE:
                return MX_CHECK_CACHE[mx], mx if MX_CHECK_CACHE[mx] else None
            smtp.connect(mx)
            MX_CHECK_CACHE[mx] = True

            try:
                smtp.quit()
            except smtplib.SMTPServerDisconnected:
                pass

            return True, mx

        except (smtplib.SMTPException, socket.timeout, ConnectionRefusedError):
            continue

        # If we failed to look up MX records, return here.


def _validate_recipient_exists(address: String, host: String,
                               timeout: int = 10) -> Union[bool, String]:
    # Internal function that utilises SMTP to test if a specified email address is
    # a valid recipient at the specified SMTP host.  It will also attempt to use
    # STARTTLS first, and fall back to plain-auth if STARTTLS fails.

    try:
        smtp = smtplib.SMTP(host, timeout=timeout)
    except smtplib.SMTPServerDisconnected:
        return False

    status, response = smtp.ehlo('testing.example.com')
    if status != 250:
        try:
            smtp.quit()
        except (smtplib.SMTPServerDisconnected, smtplib.SMTPConnectError):
            return "ERROR"

    if 'starttls' in response.decode('utf-8').lower():
        try:
            smtp.starttls()
            smtp.ehlo('testing.example.com')
        except smtplib.SMTPException:
            smtp = smtplib.SMTP(host, timeout=timeout)
            smtp.ehlo('testing.example.com')

    smtp.mail('noreply@testing.example.com')
    status, _ = smtp.rcpt(address)
    if status == 250:
        return True

    try:
        smtp.quit()
    except smtplib.SMTPException:
        pass

    return False


def validate_email_address(email: String, smtp_timeout: int = 10, validate_tld: bool = False,
                           check_mx: bool = False, verify: bool = False) -> Dict:
    """
    This function allows for email address validation according to:
     - Acceptable Syntax
     - Domain on email address is a valid TLD (optional, enforced for MX check or address verify)
     - Checking for valid, reachable MX servers/records (optional, enforced for address verify)
     - Address verification for a valid email address on MX servers (optional)

    Args:
        email (str): The email address to test.
        smtp_timeout (int): The timeout period for SMTP tests, default is 10
        validate_tld (bool): Whether or not to validate the domain as a valid TLD
        check_mx (bool): Whether or not to check for valid MX records for domain
        (forces validate_tld)
        verify (bool): Whether or not to validate whether the email address exists at
        the valid MX server(s) (forces validate_tld and check_mx)

    Returns:
        A dictionary containing validation results.  If a previous requisite check fails, the
        returned results will NOT include a validation value for that specific test.

        (Note: The examples that follow assume that all tests are enabled)

        For example, if a specified email address is syntatically correct, has a valid TLD for a
        specified domain, but has no reachable MX records or does not exist in DNS, you will get:

            {'syntax': True, 'valid_tld': True, 'mx_exists': False}

        Another example will be a syntatically correct email address but not a valid TLD:

            {'syntax': True, 'valid_tld': False}

        And for a fully valid email address, where the MX records exist, at least one MX is
        reachable, and the email address is a valid recipient:

            {'syntax': True, 'valid_tld': True, 'mx_exists': True, 'email_exists': True}

        When any of the prerequisite tests for another test to exist are not satisfied, that
        specific return value is not included (even if the test is valid, such as if
        verify is set to True, but there are no valid MX records, or if verify is True but
        the domain is not a valid TLD).
    """

    check_mx |= verify
    validate_tld |= check_mx

    validation = {"syntax": True}

    if tld:
        if validate_tld:
            validation['valid_tld'] = None

    if DNS:
        if check_mx:
            validation['mx_exists'] = None
        if verify:
            validation['email_exists'] = None

    syntax_valid = _validate_email(email, validate_tld=validate_tld)

    # If syntax is invalid, don't bother with verify, check_mx,
    # and return dict with just syntax checker result.
    if not syntax_valid['syntax']:
        validation['syntax'] = False
        for key in ['valid_tld', 'email_exists', 'mx_exists']:
            if key in validation:
                del validation[key]

        return validation

    validation['syntax'] = True

    if not tld and 'valid_tld' in validation:
        del validation['valid_tld']

    if validate_tld:
        if 'domain' in syntax_valid:
            if not syntax_valid['domain']:
                validation['valid_tld'] = False
                for key in ['email_exists', 'mx_exists']:
                    if key in validation:
                        del validation[key]

                return validation

            validation['valid_tld'] = True
    else:
        for key in ['valid_tld', 'email_exists', 'mx_exists']:
            if key in validation:
                del validation[key]

        return validation

    del syntax_valid

    if check_mx:
        mx = _validate_mx_exists(email)
        if not mx or not mx[0]:
            if 'email_exists' in validation:
                del validation['email_exists']

            validation['mx_exists'] = False

            # If we failed to look up MX records, return here
            return validation

        validation['mx_exists'] = True

    if validation['mx_exists'] and verify:
        # noinspection PyUnboundLocalVariable
        validation['email_exists'] = _validate_recipient_exists(email, mx[1], smtp_timeout)

    return validation
