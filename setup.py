from setuptools import find_packages, setup
from emailvalidate import __version__ as version

import platform
if platform.python_version_tuple()[:2] < ('3', '6'):
    raise EnvironmentError("Python versions prior to 3.6 are not supported by this library.")

setup(
    name='emailvalidate',
    version=version,
    packages=find_packages(),
    include_package_data=True,
    python_requires='>=3.6',
    install_requires=[
        'typing',
        'tld',
        'py3dns',
        'regex'
    ],
    author="Thomas Ward",
    author_email="teward@thomas-ward.net",
    description="A redesigned email validation tool, loosely based on 'validateemail' in PyPI",
    long_description="This is a library with an email validation mechanism that is inspired by "
                     "the original 'validateemail' in PyPI, but designed more for use as a library "
                     "rather than an end-user interactive tool. It extends upon the original, "
                     "and offers RFC Syntax validation, optional Valid Domain TLD verification, "
                     "optional DNS MX Record verification, and also optional verification that "
                     "the specified email address actually exists on the recipient server's end.",
    license="GPLv3",
    url="https://gitlab.com/teward/email_validate",
    download_url="https://pypi.python.org/pypi/emailvalidate",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Environment :: Other Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Communications :: Email',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    keywords='email validation',
    # test_suite='tests',
)
